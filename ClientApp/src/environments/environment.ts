// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // tslint:disable-next-line:max-line-length
  baseUrl:  'http://localhost:5100', // 'http://api.egemakina.lotusopt.com', //Change this to the address of your backend API if different from frontend address
  loginUrl: '/login',
};
