using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ege.Web.Requests;
using ege.Web.Requests.Model;

namespace ege.Web.Controllers
{
    [Route("api/[controller]")]
    public class AuditController : Controller
    {
        private readonly IMediator _mediator;

        public AuditController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Sistemdeki tüm Auditler
        /// </summary>
        /// <returns>IList</returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResultDto<AuditDto[]>), 200)]
        public async Task<ResultDto<AuditDto[]>> GetAll()
        {
            try
            {
                return await _mediator.Send(new Audit.GetAudit());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet("Avg/{st}")]
        [ProducesResponseType(typeof(ResultDto<decimal>), 200)]
        public  Task<ResultDto<decimal>> GetAvaragePh(string st)
        {
            try
            {
                return  _mediator.Send(new Audit.GetAvaragePh{ st = st});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>

        /// <summary>
        /// Audit ekle
        /// </summary>
        /// <returns>Audit</returns>
        [HttpPost("ph/{ph}/tds/{tds}")]
        [ProducesResponseType(typeof(ResultDto<AuditDto>), 200)]
        public async Task<ResultDto<AuditDto>> Create(string ph, string tds)
        {
            try
            {
                return await _mediator.Send(new Audit.SaveAudit{ Ph = decimal.Parse(ph), Tds = decimal.Parse(tds)});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    
        
        /// <summary>
        /// Audit sil
        /// </summary>
        /// <returns>Audit</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResultDto<AuditDto>), 200)]
        public async Task<ResultDto<AuditDto>> Delete(int id)
        {
            try
            {
                return await _mediator.Send(new Audit.DeleteAudit{ Id = id });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}