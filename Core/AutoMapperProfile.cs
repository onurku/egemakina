﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using ege.Web.Data.Model;
using ege.Web.Requests.Model;
using ege.Web.ViewModels;

namespace ege.Web.Core
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Ayarlar, AyarlarDto>();
            CreateMap<Product, ProductDto>();
            CreateMap<Price, SalePriceDto>();
            CreateMap<Store, StoreDto>();
            CreateMap<TCost, TCostDto>();
            CreateMap<UnitCost, UnitCostDto>();
            CreateMap<Audit, AuditDto>();


            CreateMap<ProductPostDto, Product>()
                .ForMember(m => m.Id, m => m.Ignore());
                  CreateMap<SalePricePostDto, Price>()
                .ForMember(m => m.StoreId, m => m.Ignore()).ForMember(m => m.ProductId, m => m.Ignore());
                  CreateMap<StorePostDto, Store>()
                .ForMember(m => m.Id, m => m.Ignore());
                  CreateMap<TCostPostDto, TCost>()
                .ForMember(m => m.StoreId1, m => m.Ignore()).ForMember(m => m.StoreId2, m => m.Ignore());
                  CreateMap<UnitCostPostDto, UnitCost>()
                .ForMember(m => m.Id, m => m.Ignore());
                  CreateMap<AuditPostDto, Audit>()
                .ForMember(m => m.Id, m => m.Ignore());
            CreateMap<AyarlarPostDto, Ayarlar>()
                .ForMember(m => m.Id, m => m.Ignore());

                
            CreateMap<ApplicationUser, UserViewModel>()
                   .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserViewModel, ApplicationUser>();

            CreateMap<ApplicationUser, UserEditViewModel>()
                .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserEditViewModel, ApplicationUser>();

            CreateMap<ApplicationUser, UserPatchViewModel>()
                .ReverseMap();

            CreateMap<IdentityRole, RoleViewModel>()
//                .ForMember(d => d.Permissions, map => map.MapFrom(s => s.Claims))
//                .ForMember(d => d.UsersCount, map => map.ResolveUsing(s => s.Users?.Count ?? 0))
                .ReverseMap();
            CreateMap<RoleViewModel, IdentityRole>();

            CreateMap<IdentityRoleClaim<string>, ClaimViewModel>()
                .ForMember(d => d.Type, map => map.MapFrom(s => s.ClaimType))
                .ForMember(d => d.Value, map => map.MapFrom(s => s.ClaimValue))
                .ReverseMap();

            CreateMap<ApplicationPermission, PermissionViewModel>()
                .ReverseMap();

            CreateMap<IdentityRoleClaim<string>, PermissionViewModel>()
                .ConvertUsing(s => Mapper.Map<PermissionViewModel>(ApplicationPermissions.GetPermissionByValue(s.ClaimValue)));
        }
    }
}
