using ege.Web.Data.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OpenIddict.EntityFrameworkCore.Models;

namespace ege.Web.Data {
    public class egeDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string> {
        public DbSet<Audit> Audit { get; set; }
        public DbSet<Ayarlar> Ayarlar { get; set; }

        public DbSet<Product> Product { get; set; }
        public DbSet<Price> Price { get; set; }
        public DbSet<Store> Store { get; set; }
        public DbSet<TCost> TCost { get; set; }
        public DbSet<UnitCost> UnitCost { get; set; }
        public egeDbContext (DbContextOptions options) : base (options) { }

        protected override void OnModelCreating (ModelBuilder builder) {
            base.OnModelCreating (builder);

            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.NormalizedEmail).HasMaxLength (85));
            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.NormalizedUserName).HasMaxLength (85));
            builder.Entity<IdentityRole> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityRole> (entity => entity.Property (m => m.NormalizedName).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.LoginProvider).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.ProviderKey).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserRole<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserRole<string>> (entity => entity.Property (m => m.RoleId).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.LoginProvider).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.Name).HasMaxLength (85));
            builder.Entity<IdentityUserClaim<string>> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityUserClaim<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityRoleClaim<string>> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityRoleClaim<string>> (entity => entity.Property (m => m.RoleId).HasMaxLength (85));
            builder.Entity<OpenIddictAuthorization<int>> (entity => {
                entity.Property (model => model.Subject).HasMaxLength (250);
            });
            builder.Entity<OpenIddictToken<int>> (entity => {
                entity.Property (model => model.Subject).HasMaxLength (250);
            });
        }
    }
}