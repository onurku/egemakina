using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.Audit")]
    public class Audit
    {
        public int Id { get; set; }
        [Required]
        public decimal Ph { get; set; }
        [Required]
        public decimal Tds { get; set; }

        [Column(TypeName = "datetime")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/YYYY}", ApplyFormatInEditMode = true)]
        public DateTime CreateTime { get; set; }
        
    }
}
