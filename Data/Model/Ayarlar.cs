using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.Ayarlar")]
    public class Ayarlar
    {
        [Key]        
        public int Id { get; set; }
        
        [Column(TypeName = "datetime")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/YYYY}", ApplyFormatInEditMode = true)]
        public DateTime closingTime { get; set; }
        public decimal closingInterval { get; set; }
        
        [Column(TypeName = "datetime")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/YYYY}", ApplyFormatInEditMode = true)]
        public DateTime nutrientDate { get; set; }
        public decimal phLevel { get; set; }
        
       
    }
}
