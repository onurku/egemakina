using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.Price")]
    public class Price
    {
        [Key]
        public int Id { get; set; }
    
        [Required]
        public int ProductId { get; set; }
        [Required]
        public Product Product { get; set; }
        public int StoreId { get; set; }
        [Required]
        public Store Store { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Value { get; set; }
  
    }
}
