using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.store")]
    public class Store
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Ad { get; set; }

    }
}
