using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.transportationcost")]
    public class TCost
    {
        [Key]
        public int Id { get; set; }
        public int StoreId1 { get; set; }
        [Required]
        public Store Store { get; set; }
        public int StoreId2 { get; set; }
        [Required]
        public Store Store2 { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Cost { get; set; }

    }
}
