using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ege.Web.Data.Model
{
    [Table("ege.unitcost")]
    public class UnitCost
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Required]
        public Product Product { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Cost { get; set; }

    }
}
