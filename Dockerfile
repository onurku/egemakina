# registry.gitlab.com/ekoharita/ege/api:develop
FROM microsoft/dotnet:2.2-sdk AS build-env 
WORKDIR /app 
# Copy csproj and restore as distinct layers 
COPY *.csproj ./ 
RUN dotnet restore 
# Copy everything else and build 
COPY . ./ 
RUN dotnet publish -c Release -o out 
 
# Build runtime image 
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /app 
COPY --from=build-env /app/out . 
ENV ASPNETCORE_ENVIRONMENT Production 
ENV ASPNETCORE_URLS http://*
ENTRYPOINT ["dotnet", "ege.Web.dll", "--server.urls", "http://*"] 