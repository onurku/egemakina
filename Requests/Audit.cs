using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ege.Web.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ege.Web.Requests.Model;

namespace ege.Web.Requests
{
    public static class Audit
    {
        public class GetAudit : IRequest<ResultDto<AuditDto[]>>
        {
        }

        public class GetAvaragePh : IRequest<ResultDto<decimal>>
        {
            public string st {get; set;}
        }
        
        public class SaveAudit : IRequest<ResultDto<AuditDto>>
        {
            public int Id { get; set; }
            public decimal Ph { get; set; }
            public decimal Tds { get; set; }
            
        }
        
        public class DeleteAudit : IRequest<ResultDto<AuditDto>>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<GetAudit, ResultDto<AuditDto[]>>,  
                    IRequestHandler<SaveAudit, ResultDto<AuditDto>>, IRequestHandler<DeleteAudit, ResultDto<AuditDto>>,
                    IRequestHandler<GetAvaragePh, ResultDto<decimal>>
        {
            private readonly egeDbContext _context;
            private readonly IMapper _mapper;

            public Handler(egeDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<AuditDto[]>> Handle(GetAudit request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AuditDto[]>();
                
                var list = _context.Audit.OrderByDescending(x => x.Id)
                       .Take(500);
                
                result.Result = await list.Select(x=> _mapper.Map<AuditDto>(x)).ToArrayAsync();

                return result;
            }

            public async Task<ResultDto<AuditDto>> Handle(SaveAudit request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AuditDto>();

                var entity = new Data.Model.Audit();
                entity.Ph = request.Ph;
                entity.Tds = request.Tds;
                entity.CreateTime = DateTime.UtcNow;

                if (request.Id <= default(int))
                {
                    _context.Audit.Add(entity);
                }
                else
                {
                    entity.Id = request.Id;
                    _context.Audit.Update(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<AuditDto>(entity);

                return result;
            }

            public  async Task<ResultDto<AuditDto>> Handle(DeleteAudit request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AuditDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity = new Data.Model.Audit {Id = request.Id};
                _context.Audit.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<AuditDto>(entity);

                return result;
            }

            public async Task<ResultDto<decimal>> Handle(GetAvaragePh request, CancellationToken cancellationToken)
            {
                await Task.Delay(100);
                var result = new ResultDto<decimal>();

                var list =  _context.Audit.OrderByDescending(x => x.Id)
                       .Take(100);
                if (request.st == "Ph") {
                    result.Result =  _mapper.Map<decimal>(list.Average(x => x.Ph));
                } else {
                    result.Result =  _mapper.Map<decimal>(list.Average(x => x.Tds));
                }
                return result;
            }
        }
    }
}