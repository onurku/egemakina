namespace ege.Web.Requests.Model
{
    public class AuditDto : AuditPostDto
    {
        public int id { get; set; }
    }
}