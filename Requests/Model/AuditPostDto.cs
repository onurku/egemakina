using System;

namespace ege.Web.Requests.Model
{
    public class AuditPostDto
    {
        public int Id { get; set; }
        public decimal ph { get; set; }
        public decimal tds { get; set; }
        public DateTime CreateTime { get; set; }
    }
}