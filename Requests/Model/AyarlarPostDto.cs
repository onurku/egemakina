using System;

namespace ege.Web.Requests.Model
{
    public class AyarlarPostDto
    {
        public int Id { get; set; }
        public DateTime closingTime { get; set; }
        public decimal closingInterval { get; set; }

        public DateTime nutrientDate { get; set; }
        public decimal phLevel { get; set; }
    }
}