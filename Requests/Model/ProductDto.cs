namespace ege.Web.Requests.Model
{
    public class ProductDto : ProductPostDto
    {
        public int Id { get; set; }
    }
}