﻿namespace ege.Web.Requests.Model
{
    public class ResultDto<T>
    {
        public T Result { get; set; }
        public ResultError Error { get; set; }
        public bool IsSuccessfull => Error == ResultError.None;
    }
}