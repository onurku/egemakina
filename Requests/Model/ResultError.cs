﻿namespace ege.Web.Requests.Model
{
    public enum ResultError
    {
        None,
        NotFound,
        InUse
    }
}