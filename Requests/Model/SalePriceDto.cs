namespace ege.Web.Requests.Model {
    public class SalePriceDto : SalePricePostDto {
        
        public int ProductAd { get; set; }
        public int StoreAd { get; set; }
    }
}