namespace ege.Web.Requests.Model
{
    public class SalePricePostDto
    {
       
        public int ProductId { get; set; }
        public int StoreId { get; set; }
        public decimal? Cost { get; set; }
    }
}