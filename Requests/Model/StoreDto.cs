namespace ege.Web.Requests.Model {
    public class StoreDto : StorePostDto {
        public int Id { get; set; }
    }
}