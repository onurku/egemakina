namespace ege.Web.Requests.Model {
    public class TCostDto : TCostPostDto {
        public int StoreId1Ad { get; set; }
        public int StoreId2Ad { get; set; }
    }
}