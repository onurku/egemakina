namespace ege.Web.Requests.Model
{
    public class TCostPostDto
    {
       
        public int StoreId1 { get; set; }
        public int StoreId2 { get; set; }
        public decimal? Cost { get; set; }
    }
}