namespace ege.Web.Requests.Model
{
    public class UnitCostPostDto
    {
       
        public int ProductId { get; set; }
        public decimal? Cost { get; set; }
    }
}