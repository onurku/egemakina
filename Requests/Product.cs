using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ege.Web.Data;
using ege.Web.Requests.Model;

namespace ege.Web.Requests
{
    public static class Product
    {
        public class GetProduct : IRequest<ResultDto<ProductDto[]>>
        {
        }
        
        public class SaveProduct : IRequest<ResultDto<ProductDto>>
        {
            public int Id { get; set; }
            public ProductPostDto Post { get; set; }
        }
        
        public class DeleteProduct : IRequest<ResultDto<ProductDto>>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<GetProduct, ResultDto<ProductDto[]>>, 
                    IRequestHandler<SaveProduct, ResultDto<ProductDto>>,
                    IRequestHandler<DeleteProduct, ResultDto<ProductDto>>
        {
            private readonly egeDbContext _context;
            private readonly IMapper _mapper;

            public Handler(egeDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<ProductDto[]>> Handle(GetProduct request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<ProductDto[]>();
                
                var list = _context.Product.AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<ProductDto>(x)).ToArrayAsync();

                return result;
            }

            public async Task<ResultDto<ProductDto>> Handle(SaveProduct request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<ProductDto>();
                if (request.Post == null) throw new ArgumentNullException(nameof(request.Post));

                var entity = _mapper.Map<Data.Model.Product>(request.Post);

                if (request.Id <= default(int))
                {
                    _context.Product.Add(entity);
                }
                else
                {
                    entity.Id = request.Id;
                    _context.Product.Update(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<ProductDto>(entity);

                return result;
            }

            public  async Task<ResultDto<ProductDto>> Handle(DeleteProduct request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<ProductDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity = new Data.Model.Product {Id = request.Id};
                _context.Product.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<ProductDto>(entity);

                return result;
            }
        }
    }
}