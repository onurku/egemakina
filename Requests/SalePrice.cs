using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ege.Web.Data;
using ege.Web.Requests.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ege.Web.Requests {
    public static class Price {
        public class GetPrice : IRequest<ResultDto<SalePriceDto[]>> { }

        public class SavePrice : IRequest<ResultDto<SalePriceDto>> {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public int StoreId { get; set; }
            public SalePricePostDto Post { get; set; }
        }

        public class DeletePrice : IRequest<ResultDto<SalePriceDto>> {
            public int ProductId { get; set; }
            public int StoreId { get; set; }
        }

        public class Handler : IRequestHandler<GetPrice, ResultDto<SalePriceDto[]>>,
            IRequestHandler<SavePrice, ResultDto<SalePriceDto>>,
            IRequestHandler<DeletePrice, ResultDto<SalePriceDto>> {
                private readonly egeDbContext _context;
                private readonly IMapper _mapper;

                public Handler (egeDbContext context, IMapper mapper) {
                    _context = context ??
                        throw new ArgumentNullException (nameof (context));
                    _mapper = mapper ??
                        throw new ArgumentNullException (nameof (mapper));
                }

                public async Task<ResultDto<SalePriceDto[]>> Handle (GetPrice request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SalePriceDto[]> ();

                    var list = _context.Price.Include (x => x.Store).Include (x => x.Product).AsQueryable ();

                    result.Result = await list.Select (x => _mapper.Map<SalePriceDto> (x)).ToArrayAsync ();

                    return result;
                }

                public async Task<ResultDto<SalePriceDto>> Handle (SavePrice request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SalePriceDto> ();
                    if (request.Post == null) throw new ArgumentNullException (nameof (request.Post));

                    var entity = _mapper.Map<Data.Model.Price> (request.Post);
                    if (request.Id <= default (int)) {
                        _context.Price.Add (entity);
                    } else {
                        entity.ProductId = request.ProductId;
                        entity.StoreId = request.StoreId;
                        _context.Price.Update (entity);
                    }
                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<SalePriceDto> (entity);

                    return result;
                }

                public async Task<ResultDto<SalePriceDto>> Handle (DeletePrice request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SalePriceDto> ();
                    if (request.ProductId <= default (int)) throw new ArgumentNullException (nameof (request.ProductId));
                    if (request.StoreId <= default (int)) throw new ArgumentNullException (nameof (request.StoreId));

                    var entity = new Data.Model.Price { ProductId = request.ProductId, StoreId = request.StoreId };
                    _context.Price.Remove (entity);

                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<SalePriceDto> (entity);

                    return result;
                }
            }
    }
}