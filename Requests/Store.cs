using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ege.Web.Data;
using ege.Web.Requests.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ege.Web.Requests {
    public static class Store {
        public class GetStore : IRequest<ResultDto<StoreDto[]>> { }

        public class SaveStore : IRequest<ResultDto<StoreDto>> {
            public int Id { get; set; }
            public StorePostDto Post { get; set; }
        }

        public class DeleteStore : IRequest<ResultDto<StoreDto>> {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<GetStore, ResultDto<StoreDto[]>>,
            IRequestHandler<SaveStore, ResultDto<StoreDto>>,
            IRequestHandler<DeleteStore, ResultDto<StoreDto>> {
                private readonly egeDbContext _context;
                private readonly IMapper _mapper;

                public Handler (egeDbContext context, IMapper mapper) {
                    _context = context ??
                        throw new ArgumentNullException (nameof (context));
                    _mapper = mapper ??
                        throw new ArgumentNullException (nameof (mapper));
                }

                public async Task<ResultDto<StoreDto[]>> Handle (GetStore request, CancellationToken cancellationToken) {
                    var result = new ResultDto<StoreDto[]> ();

                    var list = _context.Store.AsQueryable ();

                    result.Result = await list.Select (x => _mapper.Map<StoreDto> (x)).ToArrayAsync ();

                    return result;
                }

                public async Task<ResultDto<StoreDto>> Handle (SaveStore request, CancellationToken cancellationToken) {
                    var result = new ResultDto<StoreDto> ();
                    if (request.Post == null) throw new ArgumentNullException (nameof (request.Post));

                    var entity = _mapper.Map<Data.Model.Store> (request.Post);

                    if (request.Id <= default (int)) {
                        _context.Store.Add (entity);
                    } else {
                        entity.Id = request.Id;
                        _context.Store.Update (entity);
                    }

                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<StoreDto> (entity);

                    return result;
                }

                public async Task<ResultDto<StoreDto>> Handle (DeleteStore request, CancellationToken cancellationToken) {
                    var result = new ResultDto<StoreDto> ();
                    if (request.Id <= default (int)) throw new ArgumentNullException (nameof (request.Id));

                    var entity = new Data.Model.Store { Id = request.Id };
                    _context.Store.Remove (entity);

                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<StoreDto> (entity);

                    return result;
                }
            }
    }
}