using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ege.Web.Data;
using ege.Web.Requests.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ege.Web.Requests {
    public static class TCost {
        public class GetTCost : IRequest<ResultDto<TCostDto[]>> { }

        public class SaveTCost : IRequest<ResultDto<TCostDto>> {
            public int StoreId1 { get; set; }
            public int StoreId2 { get; set; }
            public TCostPostDto Post { get; set; }
        }

        public class DeleteTCost : IRequest<ResultDto<TCostDto>> {
            public int StoreId1 { get; set; }
            public int StoreId2 { get; set; }
        }

        public class Handler : IRequestHandler<GetTCost, ResultDto<TCostDto[]>>,
            IRequestHandler<SaveTCost, ResultDto<TCostDto>>,
            IRequestHandler<DeleteTCost, ResultDto<TCostDto>> {
                private readonly egeDbContext _context;
                private readonly IMapper _mapper;

                public Handler (egeDbContext context, IMapper mapper) {
                    _context = context ??
                        throw new ArgumentNullException (nameof (context));
                    _mapper = mapper ??
                        throw new ArgumentNullException (nameof (mapper));
                }

                public async Task<ResultDto<TCostDto[]>> Handle (GetTCost request, CancellationToken cancellationToken) {
                    var result = new ResultDto<TCostDto[]> ();

                    var list = _context.TCost.AsQueryable ();

                    result.Result = await list.Select (x => _mapper.Map<TCostDto> (x)).ToArrayAsync ();

                    return result;
                }

                public async Task<ResultDto<TCostDto>> Handle (SaveTCost request, CancellationToken cancellationToken) {
                    var result = new ResultDto<TCostDto> ();
                    if (request.Post == null) throw new ArgumentNullException (nameof (request.Post));

                    var entity = _mapper.Map<Data.Model.TCost> (request.Post);

                    if (request.StoreId1 <= default (int)) {
                        _context.TCost.Add (entity);
                    } else {
                        entity.StoreId1 = request.StoreId1;
                        entity.StoreId2 = request.StoreId2;
                        _context.TCost.Update (entity);
                    }

                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<TCostDto> (entity);

                    return result;
                }

                public async Task<ResultDto<TCostDto>> Handle (DeleteTCost request, CancellationToken cancellationToken) {
                    var result = new ResultDto<TCostDto> ();
                    if (request.StoreId1 <= default (int)) throw new ArgumentNullException (nameof (request.StoreId1));
                    if (request.StoreId2 <= default (int)) throw new ArgumentNullException (nameof (request.StoreId2));

                    var entity = new Data.Model.TCost { StoreId1 = request.StoreId1 , StoreId2 = request.StoreId2 };
                    _context.TCost.Remove (entity);

                    await _context.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<TCostDto> (entity);

                    return result;
                }
            }
    }
}