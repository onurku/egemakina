using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ege.Web.Data;
using ege.Web.Requests.Model;

namespace ege.Web.Requests
{
    public static class UnitCost
    {
        public class GetUnitCost : IRequest<ResultDto<UnitCostDto[]>>
        {
        }
        
        public class GetByProductIdUnitCost : IRequest<ResultDto<UnitCostDto>>
        {
            public int ProductId { get; set; }
        }
        
        public class SaveUnitCost : IRequest<ResultDto<UnitCostDto>>
        {
            public int Id { get; set; }
            public UnitCostPostDto Post { get; set; }
        }
        
        public class DeleteUnitCost : IRequest<ResultDto<UnitCostDto>>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<GetUnitCost, ResultDto<UnitCostDto[]>>, IRequestHandler<GetByProductIdUnitCost, ResultDto<UnitCostDto>>, 
                    IRequestHandler<SaveUnitCost, ResultDto<UnitCostDto>>, IRequestHandler<DeleteUnitCost, ResultDto<UnitCostDto>>
        {
            private readonly egeDbContext _context;
            private readonly IMapper _mapper;

            public Handler(egeDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<UnitCostDto[]>> Handle(GetUnitCost request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<UnitCostDto[]>();
                
                var list = _context.UnitCost.Include("Grup").AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<UnitCostDto>(x)).ToArrayAsync();

                return result;
            }

            public async Task<ResultDto<UnitCostDto>> Handle(SaveUnitCost request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<UnitCostDto>();
                if (request.Post == null) throw new ArgumentNullException(nameof(request.Post));

                var entity = _mapper.Map<Data.Model.UnitCost>(request.Post);

                if (request.Id <= default(int))
                {
                    _context.UnitCost.Add(entity);
                }
                else
                {
                    entity.Id = request.Id;
                    _context.UnitCost.Update(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<UnitCostDto>(entity);

                return result;
            }

            public  async Task<ResultDto<UnitCostDto>> Handle(DeleteUnitCost request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<UnitCostDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity = new Data.Model.UnitCost {Id = request.Id};
                _context.UnitCost.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<UnitCostDto>(entity);

                return result;
            }

            public async Task<ResultDto<UnitCostDto>> Handle(GetByProductIdUnitCost request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<UnitCostDto>();
                if(request.ProductId <= default(int)) throw new ArgumentNullException(nameof(request.ProductId));
                
                var entity = await _context.UnitCost.Include("Product").FirstOrDefaultAsync(x=> x.ProductId == request.ProductId, cancellationToken: cancellationToken);

                if (entity != null)
                {
                    result.Result = _mapper.Map<UnitCostDto>(entity);   
                }
                else
                {
                    result.Error = ResultError.NotFound;
                }
                
                return result;
            }

        }
    }
}